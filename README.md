# CS371p: Object-Oriented Programming Collatz Repo

* Name: Brandt Swanson

* EID: bas4932

* GitLab ID: brandtswan

* HackerRank ID: brandtswan1

* Git SHA: 8c746bf60ad2ce594e90849634f80390a3b8c4a5

* GitLab Pipelines: https://gitlab.com/brandtswan/cs371p-collatz/-/pipelines/253961301

* Estimated completion time: 30

* Actual completion time: 18

* Comments: This project really helped me feel more comfortable with C++ and with the different tools we use within this class. I'm looking forward to continuing this growth!
