// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

//Change

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(131, 19)), make_tuple(131, 19, 122));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(3640, 809745)), make_tuple(3640, 809745, 509));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(499568, 836525)), make_tuple(499568, 836525, 509));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(257666, 387083)), make_tuple(257666, 387083, 441));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(440384, 267749)), make_tuple(440384, 267749, 449));
}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1)), make_tuple(1, 1, 1));
}

TEST(CollatzFixture, eval10) {
    ASSERT_EQ(collatz_eval(make_pair(123121, 870568)), make_tuple(123121, 870568, 525));
}

TEST(CollatzFixture, eval11) {
    ASSERT_EQ(collatz_eval(make_pair(443656, 32860)), make_tuple(443656, 32860, 449));
}

TEST(CollatzFixture, eval12) {
    ASSERT_EQ(collatz_eval(make_pair(895090, 478530)), make_tuple(895090, 478530, 525));
}

TEST(CollatzFixture, eval13) {
    ASSERT_EQ(collatz_eval(make_pair(6288, 134108)), make_tuple(6288, 134108, 354));
}

TEST(CollatzFixture, eval14) {
    ASSERT_EQ(collatz_eval(make_pair(39211, 542111)), make_tuple(39211, 542111, 470));
}

TEST(CollatzFixture, eval15) {
    ASSERT_EQ(collatz_eval(make_pair(352017, 6064)), make_tuple(352017, 6064, 443));
}

TEST(CollatzFixture, eval16) {
    ASSERT_EQ(collatz_eval(make_pair(532250, 116830)), make_tuple(532250, 116830, 470));
}

TEST(CollatzFixture, eval17) {
    ASSERT_EQ(collatz_eval(make_pair(763931, 787583)), make_tuple(763931, 787583, 468));
}

TEST(CollatzFixture, eval18) {
    ASSERT_EQ(collatz_eval(make_pair(129272, 195075)), make_tuple(129272, 195075, 383));
}

TEST(CollatzFixture, eval19) {
    ASSERT_EQ(collatz_eval(make_pair(229058, 533065)), make_tuple(229058, 533065, 470));
}

TEST(CollatzFixture, eval20) {
    ASSERT_EQ(collatz_eval(make_pair(29, 39)), make_tuple(29, 39, 107));
}

TEST(CollatzFixture, eval21) {
    ASSERT_EQ(collatz_eval(make_pair(707082, 347046)), make_tuple(707082, 347046, 509));
}

TEST(CollatzFixture, eval22) {
    ASSERT_EQ(collatz_eval(make_pair(178111, 72459)), make_tuple(178111, 72459, 383));
}

TEST(CollatzFixture, eval23) {
    ASSERT_EQ(collatz_eval(make_pair(418967, 205)), make_tuple(418967, 205, 449));
}

TEST(CollatzFixture, eval24) {
    ASSERT_EQ(collatz_eval(make_pair(414529, 368568)), make_tuple(414529, 368568, 449));
}


// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
